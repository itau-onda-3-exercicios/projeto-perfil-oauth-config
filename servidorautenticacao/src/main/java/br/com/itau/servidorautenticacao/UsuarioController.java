package br.com.itau.servidorautenticacao;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping("/usuario")
	public Usuario inserir(@RequestBody Usuario usuario) {
		return usuarioService.inserir(usuario);
	}
	
	@GetMapping("/validar")
	public Map<String, String> validar(Principal principal) {
		Map<String, String> mapa = new HashMap<>();
		mapa.put("name", principal.getName());
		
		return mapa;
	}
}
